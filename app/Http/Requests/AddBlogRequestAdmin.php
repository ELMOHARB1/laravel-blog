<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddBlogRequestAdmin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:6|max:250',
            'short_desc' => 'required|string|min:6|max:500',
            'content' => 'required|string|min:6',
            'image' => 'required|image|max:2048',
            'category_id' => 'required'
        ];
    }
}
