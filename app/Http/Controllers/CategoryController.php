<?php

namespace App\Http\Controllers;


use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests\AddCategoryRequestAdmin;

class CategoryController extends Controller
{
    //


    public function index(){

        $categories = Category::all();

        return view('admin.category.index',compact('categories'));

    }


    public function create(){

        return view('admin.category.add');

    }



    public function store(AddCategoryRequestAdmin $request, Category $category){

        $category->create([
            'name' => $request->name,
        ]);

        return redirect('adminpanel/category/create')->with('success', 'تمت اضافة قسم جديد بنجاح');

    }


    public function edit($id){

        $category =  Category::find($id);

        return view('admin.category.edit',compact('category'));

    }


    public function update($id,AddCategoryRequestAdmin $request){

        $categoryUpdated = Category::find($id);

        $categoryUpdated -> fill($request->all())->save();

        return redirect('adminpanel/category/'.$categoryUpdated->id.'/edit')->with('success', 'تم تعديل القسم بنجاح');

    }


    public function destroy($id){

        $category = Blog::find($id);
        $category->delete();

        return redirect('adminpanel/category')->with('success', 'تم حذف القسم بنجاح');
    }




}
