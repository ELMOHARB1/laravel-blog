<?php

namespace App\Http\Controllers;

use DB;
use App\Comments;
use Illuminate\Http\Request;
use App\Http\Requests\AddCommentRequest;

class CommentController extends Controller
{
    //

    public function store(AddCommentRequest $request){

        $comment = new Comments ;

        $comment->name = $request->name;
        $comment->comment = $request->comment;
        $comment->blog_id = $request->blog_id;

        $comment->save();

        return $comment;
    }




    function load_data(Request $request)
    {
        if($request->ajax())
        {
            if($request->id > 0)
            {
                $data = DB::table('comments')
                    ->where('blog_id','=', $request->blog_id)
                    ->where('id', '<', $request->id)
                    ->orderBy('id', 'DESC')
                   // ->limit(4) //in case load more than once
                    ->get();

            }
            else
            {
                $data = DB::table('comments')
                    ->where('blog_id','=', $request->blog_id)
                    ->orderBy('id', 'DESC')
                    ->limit(4)
                    ->get();


            }
            $output = '';
            $last_id = '';

            if(!$data->isEmpty())
            {
                $last_row_db = DB::table('comments')
                    ->where('blog_id','=', $request->blog_id)
                    ->orderBy('id', 'ASC')
                    ->first();
                $last_id_db = $last_row_db->id;

                $current_count = count($data);
                $count_all = DB::table('comments')->where('blog_id','=', $request->blog_id)->count();
                $the_rest = ($count_all-$current_count);

                foreach($data as $row)
                {
                    $output .= '<div class="row">
                         <div class="col-md-10">
                             <div class="block-heading-two">
                                 <p><span>  <i  class="fa fa-user"></i> ' . $row->name . ' </span>
                                     <span style="float: left;"><i  class="fa fa-clock-o"></i> ' . $row->created_at . ' </span>
                                 </p>
                                 <hr>
                             </div>
                             <p>'
                        .$row->comment.
                        '</p>
                     </div>
                     <div class="col-md-2">
                         <img  class="img-bordered" width="100" height="100" src="https://secure.gravatar.com/avatar/ceceb2d0dca259990c1e92b2ae1a59c8?s=100&d=mm&r=g" >
                     </div>
                 </div>
                 <br><br><br>';
                    $last_id = $row->id;
                }
        if($last_id !== $last_id_db){
                $output .= '
       <div id="load_more" data-last="'.$last_id_db.'" data-id="'.$last_id.'">
        <button type="button" name="load_more_button" class="btn btn-success form-control" data-last="'.$last_id_db.'" data-id="'.$last_id.'" id="load_more_button"> تحميل عدد '. $the_rest .'   من التعليقات </button> 
       </div>
       ';}
            }
            else
            {

                    $output .= '';


            }
            echo $output;
        }
    }
//<div id="load_more">
//<button type="button" name="load_more_button" class="btn btn-info form-control">لا توجد تعليقات اخرى</button>
//</div>

}
