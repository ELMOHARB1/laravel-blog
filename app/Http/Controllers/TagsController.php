<?php

namespace App\Http\Controllers;

use App\Tags;
use Illuminate\Http\Request;
use App\Http\Requests\AddTagsRequestAdmin;

class TagsController extends Controller
{
    //


    public function index(){

        $tags = Tags::all();

        return view('admin.tags.index',compact('tags'));

    }


    public function create(){

        return view('admin.tags.add');

    }



    public function store(AddTagsRequestAdmin $request, Tags $tags){

        $tags->create([
            'name' => $request->name,
        ]);

        return redirect('adminpanel/tags/create')->with('success', 'تمت اضافة تاج جديد بنجاح');

    }


    public function edit($id){

        $tag =  Tags::find($id);

        return view('admin.tags.edit',compact('tag'));

    }


    public function update($id,AddTagsRequestAdmin $request){

        $tagsUpdated = Tags::find($id);

        $tagsUpdated -> fill($request->all())->save();

        return redirect('adminpanel/tags/'.$tagsUpdated->id.'/edit')->with('success', 'تم تعديل التاج بنجاح');

    }


    public function destroy($id){

        $tag = Tags::find($id);
        $tag -> delete();
        return redirect('adminpanel/tags')->with('success', 'تم حذف التاج بنجاح');
    }

}
