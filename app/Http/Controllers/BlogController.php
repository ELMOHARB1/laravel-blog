<?php

namespace App\Http\Controllers;
use DB;
use App\Tags;
use App\Category;
use App\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests\AddBlogRequestAdmin;
use App\Http\Requests\UpdateBlogRequestAdmin;

class BlogController extends Controller
{


    public function index(){

       $blogs = Blog::all();

      // print_r($blogs);die;

        return view('admin.blog.index',compact('blogs'));

    }


    public function create(){

        $categories = Category::all();
        $tags = Tags::all();

        return view('admin.blog.add',compact('categories','tags'));

    }



    public function store(AddBlogRequestAdmin $request, Blog $blog){

       // print_r($request->tag_id);die;

        //upload
        $file = $request->file('image');
        $distnationpath = 'images';
       // $filename = $file->getClientOriginalName();
        $file_ext = $file->getClientOriginalExtension();
        $filename_new = uniqid('',true).time().'.'.$file_ext;
        $file->move($distnationpath,$filename_new);


        $blog->title = $request->title;
        $blog->short_desc = $request->short_desc;
        $blog->content = $request->content;
        $blog->image = $filename_new;
        $blog->category_id = $request->category_id;
        $blog->save();


        if(!empty($request->tag_id)) {

            $blog->tags()->attach(array_values($request->tag_id));

        }


        return redirect('adminpanel/blog/create')->with('success', 'تمت اضافة مدونه بنجاح');

    }


    public function edit($id){

        $blog =  Blog::find($id);
        //dd($blog);
        $categories = Category::all();
        $tags = Tags::all();
        //print_r(array_values((array)$blog->tags()->allRelatedIds()));die;

        return view('admin.blog.edit',compact('blog','categories','tags'));

    }


    public function update($id,UpdateBlogRequestAdmin $request){

        $blogUpdated = Blog::find($id);

       // print_r($blogUpdated->tags()->allRelatedIds());die;

        //upload
        $file = $request->file('image');
        if(!empty($file)) {
            //delete old image
            $old_image = 'images/'.$blogUpdated->image;
            if(is_file($old_image)){unlink($old_image);}
            $distnationpath = 'images';
           // $filename = $file->getClientOriginalName();
            $file_ext = $file->getClientOriginalExtension();
            $filename_new = uniqid('',true).time().'.'.$file_ext;
            $file->move($distnationpath, $filename_new);
            $blogUpdated->image = $filename_new;
        }

        $blogUpdated->title = $request->title;
        $blogUpdated->short_desc = $request->short_desc;
        $blogUpdated->content = $request->content;
        $blogUpdated->category_id = $request->category_id;

        $blogUpdated->save();

        if(!empty($request->tag_id)){
            $tag_ids =  array_values($request->tag_id);
            $blogUpdated->tags()->sync($tag_ids);
        }else{
            $blogUpdated->tags()->detach($blogUpdated->tags);
        }

       // $blogUpdated -> fill($request->all())->save();

        return redirect('adminpanel/blog/'.$blogUpdated->id.'/edit')->with('success', 'تم تعديل المدونه بنجاح');

    }


    public function destroy($id){

        $blog = Blog::find($id);
        //unlink tags
        $blog->tags()->detach($blog->tags);
        //delete image
        $image = 'images/'.$blog->image;
        if(is_file($image)){unlink($image);}

        $blog->delete();

        return redirect('adminpanel/blog')->with('success', 'تم حذف المدونه بنجاح');
    }

    public function bolgCategory($id){

     $blog = new Blog;

     $blogs = $blog->categoryById($id);

       return view('admin.blog.index',compact('blogs'));

    }


    public function tagBlog($tag){

        $blogs = Blog::whereHas('tags', function($query)use($tag){
            return $query->where('name',$tag);
        })->get();

        return view('admin.blog.index',compact('blogs'));

    }





//front side


       public function home(){

        $blogs =  DB::table('blog')->orderBy('created_at', 'desc')->take(4)->get();
        //print_r($blogs);die;
        $categories = Category::all();

        return view('welcome',compact('blogs','categories'));

       }


        public function allBlog(){
          $blogs =  Blog::orderBy('id', 'DESC')->paginate(3);
          $cats = Category::all();
          $tags = Tags::all();
          return view('blog.all',compact('blogs','cats','tags'));
        }


        public function oneBlog($id){
            $blog =  Blog::find($id);
            $cats = Category::all();
            $tags = Tags::all();
            return view('blog.one',compact('blog','tags','cats'));
        }






    public function blogSearch($text = null, Request $request){

        if(!empty($request->search))
        $text = $request->search;

        $blog = Blog::orderBy('id', 'DESC')->where('title','like','%'.$text.'%')->paginate(3);

        $blogTags = Blog::orderBy('id', 'DESC')->whereHas('tags', function($query)use($text){
            return $query->where('name','like','%'.$text.'%');
        })->paginate(3);

        $blogCats = Blog::orderBy('id', 'DESC')->whereHas('category', function($query)use($text){
            return $query->where('name','like','%'.$text.'%');
        })->paginate(3);

        $total = $blog->total() + $blogTags->total() + $blogCats->total();

        $items = array_merge($blog->items(),$blogCats->items(),$blogTags->items());

        $itemsCollection = collect($items)->unique();

        //$count = count($itemsCollection) == 0 ? 1 : count($itemsCollection);

        $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage();

        $blogs = new \Illuminate\Pagination\LengthAwarePaginator($itemsCollection, $total, 3, $currentPage);


        $cats = Category::all();
        $tags = Tags::all();

        return view('blog.all',compact('blogs','text', 'cats','tags'));

    }


    public static function  render_comments($id)
    {
        $blog = Blog::find($id);

        if (!empty($blog->comments)){

            foreach ($blog->comments as $comment) {
            echo '<div class="row">
                         <div class="col-md-10">
                             <div class="block-heading-two">
                                 <p><span>  <i  class="fa fa-user"></i> ' . $comment->name . ' </span>
                                     <span style="float: left;"><i  class="fa fa-clock-o"></i> ' . $comment->created_at . ' </span>
                                 </p>
                                 <hr>
                             </div>
                             <p>'
                    .$comment->comment.
                    '</p>
                     </div>
                     <div class="col-md-2">
                         <img  class="img-bordered" width="100" height="100" src="https://secure.gravatar.com/avatar/ceceb2d0dca259990c1e92b2ae1a59c8?s=100&d=mm&r=g" >
                     </div>
                 </div>
                 <br><br><br>';
            }
    }

    }






}
