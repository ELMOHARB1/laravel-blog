<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\AddUserRequestAdmin;
use Illuminate\Support\Facades\Hash;


class UsersController extends Controller
{
    public function index(User $user){

        $users = $user->all();

        return view('admin.user.index',compact('users'));
    }


    public function create(){

        return view('admin.user.add');
    }


    public function store(AddUserRequestAdmin $request, User $user)
    {

         $user->create([
            'name' => $request->name,
            'email' => $request->email,
            'admin' => $request->admin,
            'password' => Hash::make($request->password),
        ]);

        return redirect('adminpanel/users/create')->with('success', 'تمت اضافة المستخدم بنجاح');
    }

    public function edit($id){

        $user = User::find($id);

        return view('admin.user.edit',compact('user'));
    }






    public function update($id,Request $request)
    {
        $uniqueEmail = ' ';
        $userUpdated = User::find($id);


        if($request->email != $userUpdated->email) {
            $uniqueEmail = '|unique:users';
        }

        $this->validate($request,array(
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255'.$uniqueEmail
        ));

        $userUpdated -> fill($request->all())->save();

        return redirect('adminpanel/users/'.$userUpdated->id.'/edit')->with('success', 'تم تعديل العضو بنجاح');
    }


    public function updatePassword(Request $request)
    {
        //print_r($request);die;
        $id = $request->user_id;

        $userUpdated = User::find($id);

        $this->validate($request, array(
            'password' => 'required|string|min:6'
        ));

        // print_r($password);die;
        $password = Hash::make($request->password);

        $userUpdated->password = $password;
        $userUpdated->save();

        return redirect('adminpanel/users/' . $userUpdated->id . '/edit')->with('success', 'تم تعديل كلمة المرور بنجاح');


    }

    public function destroy($id){

       $user = User::find($id);
       $user->delete();

        return redirect('adminpanel/users')->with('success', 'تم حذف المستخدم بنجاح');
    }


}
