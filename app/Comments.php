<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    //
  protected $table = 'comments';

    protected $fillable = [
       'name','comment','blog_id'
    ];

    function blog(){
        return $this->belongsTo('\App\Blog');
    }

}
