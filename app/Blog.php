<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blog';

    protected $fillable = [
        'title', 'short_desc', 'content','image','category_id'
    ];


    function category(){
        return $this->belongsTo('App\Category','category_id');
    }


    function categoryById($id){
        return $this->where('category_id',$id)->get();
    }

    function tags(){
        return $this->belongsToMany('App\Tags','link_tags','blog_id','tag_id')->withTimestamps();
    }

    function comments(){
        return $this->hasMany('App\Comments','blog_id');
    }





}
