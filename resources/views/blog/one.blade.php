@extends('layouts.app')
@section('title')
    {!!\Illuminate\Support\Str::words($blog->title,10,'..') !!}
@endsection




@section('content')

    <div class="about">
        <div class="container">
            <section class="title-section">
                <h1 class="title-header">المدونه
                    @if(!empty($blog))
                        / {!!\Illuminate\Support\Str::words($blog->title,10,'..') !!}
                    @endif
                </h1>

            </section>
        </div>
    </div>


    <div class="container">

        <div class="row">

            {{--side bar--}}

            <div class="col-lg-3">
                @if(!empty($cats))
                    <div class="block-heading-two">
                        <h3><span><i class="fa fa-flag"></i> الأقسام </span></h3>
                    </div>
                    <div class="panel-group" id="accordion-alt3">
                        @foreach($cats as $cate)
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a  href="{{url('blog/'.$cate->name)}}">  {{$cate->name}} <i class="fa fa-angle-left"></i></a> </h4>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
                @if(!empty($tags))
                    <div class="block-heading-two">
                        <h3><span> <i class="fa fa-tags"></i> التاجات </span></h3>
                    </div>

                    @foreach($tags as $tag)
                        <a href="{{url('blog/'.$tag->name)}}"> <span style="font-size: 15px;background-color: #23a17e;" class="badge badge-info"> {{$tag->name}} </span> </a>
                    @endforeach

                @endif
            </div>

            {{--side bar end--}}

                 <div class="col-lg-9">
                     <div class="row">


            <div class="col-lg-12">
                <img  class="img-bordered" width="100%" height="483" src="{{url('images/'.$blog->image)}}" >
            </div>
                     </div>
                     <br>

                     <div class="row">
            <div class="col-lg-12">

                <div class="block-heading-two">
                    <p>
                        <i  class="fa fa-clock-o"></i><span> {{$blog->created_at}}  -   </span>
                        <i  class="fa fa-flag"></i> <a href="{{url('blog/'.$blog['category']->name)}}"> {{$blog['category']->name}} </a> <span>  -  </span>
                        <i class="fa fa-comments-o"></i><span> {{count($blog->comments)}}  </span>
                        <br>

                    </p>
                    <br>
                    <h1><span>{{$blog->title}}</span></h1>
                </div>

                <p>
                    {{$blog->content}}

                </p>
                <br>
                <p>
                    @if(!empty($blog->tags))
                        <i class="fa fa-tags"></i>
                        @foreach($blog->tags as $tag)
                            <a href="{{url('blog/'.$tag->name)}}"> <span style="font-size: 15px;background-color: #23a17e;" class="badge bg-success"> {{$tag->name}} </span> </a>
                        @endforeach
                    @endif
                </p>


            </div>
                     </div>
                     <br>
                     <hr>



                     <div class="row">
                         <div class="col-lg-12">
                             <div class="block-heading-two" >
                                 <h1><span>اضف تعليقك</span><span style="float: left;"><i class="fa fa-comments-o"></i> {{count($blog->comments)}} </span></h1>
                             </div>
                             <br>

                             <div class="row">

                                 {{--<div class="alert alert-danger fade in alert-dismissible" id="errorsMsgs">--}}
                                     {{--<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>--}}

                                 {{--</div>--}}
               {{--<div class="alert alert-danger" id="errorsMsgs" ></div>--}}



{!! Form::open(array('url'=>'store','method'=>'post','id'=>'createComment')) !!}
    {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'الاسم']) !!}<br>
    {!! Form::hidden('blog_id',$blog->id) !!}
    {!! Form::textarea('comment',null,['class'=>'form-control','placeholder'=>'التعليق']) !!}<br>

    {!! Form::submit('اضف تعليق',['class'=>'btn btn-info']) !!}<br>

{!! Form::close() !!}

                             </div>


                         </div>
                     </div>
                     <br>
                     <br>

                     <div class="row">
                         <div class="col-lg-12">
                             {{--<div class="block-heading-two">--}}
                                 {{--<h1><span>التعليقات</span> </h1>--}}
                             {{--</div>--}}


                             <diV id="comments_body">

                                 {{--{{\App\Http\Controllers\BlogController::render_comments($blog->id)}}--}}

                             </diV>
                             {{--@if(!empty($blog->comments))--}}
                             {{--@foreach($blog->comments as $comment)--}}
                             {{----}}
                             {{--<div class="row">--}}
                             {{--<div class="col-md-10">--}}
                             {{--<div class="block-heading-two">--}}
                             {{--<p>--}}
                             {{--<span>  <i  class="fa fa-user"></i>  {{$comment->name}}  </span>--}}
                             {{--<span style="float: left;"><i  class="fa fa-clock-o"></i> {{$comment->created_at}}     </span>--}}
                             {{--</p>--}}
                             {{--<hr>--}}
                             {{--</div>--}}
                             {{--<p>--}}
                             {{--{{$comment->comment}}--}}
                             {{--</p>--}}
                             {{--</div>--}}
                             {{--<div class="col-md-2">--}}
                             {{--<img  class="img-bordered" width="100" height="100" src="https://secure.gravatar.com/avatar/ceceb2d0dca259990c1e92b2ae1a59c8?s=100&d=mm&r=g" >--}}
                             {{--</div>--}}
                             {{--</div>--}}

                             {{--<br> <br>  <br>--}}
                             {{--</diV>--}}
                             {{--@endforeach--}}
                             {{--@endif--}}



                         </div>

                     </div>


                 </div>
        </div>
    </div>







    </div>

    <br>





@endsection


@section('footer')


    <script>
        $(document).ready(function(){

            var _token = $('input[name="_token"]').val();
            var blog_id = $('input[name="blog_id"]').val();

            load_data('',blog_id, _token);

            function load_data(id="", blog_id, _token)
            {
                $.ajax({
                    url:"{{ route('comment.load_data') }}",
                    method:"POST",
                    data:{id:id, blog_id:blog_id ,_token:_token},
                    success:function(data)
                    {
                        $('#load_more_button').remove();
                        $('#comments_body').append(data);
                    }
                })
            }

            $(document).on('click', '#load_more_button', function(){
                var id = $(this).data('id');
                var last_id = $(this).data('last');

                $('#load_more_button').html('<b>جارى التحميل ... </b>');
                load_data(id, blog_id, _token);



            });

        });
    </script>



    <script>
        $(function(){
           // $('#errorsMsgs').hide();

            $("#createComment").submit(function(e){
                e.preventDefault();

                var data = new FormData();

                data.append('_token',$('input[name="_token"]').val());
                data.append('name',$('input[name="name"]').val());
                data.append('comment',$('textarea[name="comment"]').val());
                data.append('blog_id',$('input[name="blog_id"]').val());

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:"{{ url('comment/create') }}",
                    type:'POST',
                    data: data,
                    contentType: false,
                    dataType : "json",
                    processData:false,
                    success:function(data){
                        $.toast({
                            text: 'تم التعليق',
                            position: 'top-right',
                            hideAfter: 4000,
                            showHideTransition: 'fade',
                            stack: false,
                            textAlign: 'right',
                            allowToastClose: false,
                            icon: 'success'
                        });

                        $('#comments_body').prepend(
                            '<div class="row">'+
                            '<div class="col-md-10">'+
                            '<div class="block-heading-two">'+
                            '<p><span><i  class="fa fa-user"></i> '+ data.name +' </span>'+
                            '<span style="float: left;"><i  class="fa fa-clock-o"></i> '+ data.created_at +' </span>' +
                            '</p>'+
                            '<hr>'+
                            '</div>'+
                            '<p>'+data.comment+'</p>'+
                            '</div>'+
                            '<div class="col-md-2">'+
                            '<img  class="img-bordered" width="100" height="100" src="https://secure.gravatar.com/avatar/ceceb2d0dca259990c1e92b2ae1a59c8?s=100&d=mm&r=g" >'+
                            '</div>' +
                            '</div>'+
                            '<br><br><br>');

                        $('#createComment')[0].reset();
                        },
                    error:function (data) {
                        //$('#errorsMsgs').show();
                        //$('#errorsMsgs').html('');
                        var resp = JSON.parse(data.responseText);
                        var errors = resp.errors;
                        var tosts = new Array();
                        $.each(errors ,function (k,v) {
                            //$('#errorsMsgs').append(v+"<br>");
                              tosts.push("<b>"+v+"<br></b>");
                        });

                        $.toast({
                            heading:'خطأ !',
                            text: tosts,
                            position: 'top-right',
                            hideAfter: 6000,
                            showHideTransition: 'fade',
                            stack: false,
                            textAlign: 'right',
                            allowToastClose: false,
                            icon: 'error'
                        });


                    }

                });

            });
        });
    </script>




@endsection

