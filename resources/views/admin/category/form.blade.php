

    <div class="form-group row">
        <label style="float: none;" for="name" class="col-md-6 text-right"> اسم القسم</label>

        <div style="float: none;" class="col-md-6">
            {!! Form::text("name",null,['class'=>'form-control']) !!}

            @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div class="form-group row mb-0">
        <div style="float: none;"  class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-success">
                حفظ
            </button>
        </div>
    </div>


