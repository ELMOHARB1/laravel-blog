
@extends('admin.layouts.layout')

@section('title')
    تعديل القسم
    {{$category->name}}
@endsection

@section('header')

@endsection

@section('content')

    <section class="content-header">
        <h1>
            الاقسام
            <small>   تعديل القسم  {{ $category->name }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> الرئيسيه</a></li>
            <li><a href="{{url('/adminpanel/category')}}"><i class="fa fa-users"></i> عرض الاقسام</a></li>

            <li class="active">
                  تعديل القسم</li>
            {{$category->name}}
        </ol>
    </section>


    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">تعديل القسم</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">


                       {!! Form::model($category,array('route' =>array('category.update',$category->id), 'method'=>'patch')) !!}

                            @include('admin.category.form')

                       {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


@section('footer')

@endsection