@extends('admin.layouts.layout')

@section('title')
    الرئيسيه
@endsection

@section('content')
<div class="content" >
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box " >
                <span class="info-box-icon bg-aqua"><i class="fa fa-comment-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">التعليقات</span>
                    <span class="info-box-number">{{count(\App\Comments::all())}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-book"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">الأقسام</span>
                    <span class="info-box-number">{{count(\App\Category::all())}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-tags"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">التاجات</span>
                    <span class="info-box-number">{{count(\App\Tags::all())}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-newspaper-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">التدوينات</span>
                    <span class="info-box-number">{{count(\App\Blog::all())}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <iframe onclick="javascript(0);" class="center-block" src="http://free.timeanddate.com/clock/i6v42c5i/n53/szw210/szh210/hoc09f/hbw6/hfceee/cf100/hnc9ff/hwc000/hcceee/hcw2/hcd88/fan2/fas20/fdi70/mqc000/mqs2/mql13/mqw4/mqd94/mhc000/mhs3/mhl13/mhw4/mhd94/mmc000/mms2/mml5/mmw1/mmd94/hwm2/hhs2/hhb18/hms2/hml70/hmb18/hmr7/hscf09/hss1/hsl90/hsr5" frameborder="0" width="210" height="210"></iframe>

</div>
    <!-- =========================================================== -->
@endsection