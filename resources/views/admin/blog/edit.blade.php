
@extends('admin.layouts.layout')

@section('title')
    تعديل المدونه
    {{$blog->title}}
@endsection

@section('header')
    <style>

        .select2-container--default[dir="rtl"] .select2-selection--multiple .select2-selection__choice__remove{
            margin-left: 7px;
        }

    </style>
@endsection

@section('content')

    <section class="content-header">
        <h1>
            المدونات
            <small>   تعديل المدونه  {{ $blog->title }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> الرئيسيه</a></li>
            <li><a href="{{url('/adminpanel/blog')}}"><i class="fa fa-users"></i> عرض المدونات</a></li>

            <li class="active">
                  تعديل المدونه</li>
            {{$blog->title}}
        </ol>
    </section>


    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">تعديل محتوى المدونه</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">


                       {!! Form::model($blog,array('route' =>array('blog.update',$blog->id), 'method'=>'patch','files'=>'true')) !!}

                            @include('admin.blog.form')

                       {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('footer')

    <!-- Select2 -->
    {!! Html::script('/admin/bower_components/select2/dist/js/select2.full.min.js') !!}
    <script>
        $(".select_tags").select2({
            dir: "rtl",
        });
    </script>

@endsection