@extends('admin.layouts.layout')

@section('title')
    اضافة مدونه
@endsection

@section('header')

    <style>

        .select2-container--default[dir="rtl"] .select2-selection--multiple .select2-selection__choice__remove{
            margin-left: 7px;
        }

    </style>

@endsection

@section('content')

    <section class="content-header">
        <h1>
            المدونات
            <small>اضافة مدونه</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> الرئيسيه</a></li>
            <li><a href="{{url('/adminpanel/blog')}}"><i class="fa fa-users"></i> عرض المدونات</a></li>

            <li class="active">اضافة مدونه</li>

        </ol>
    </section>


    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <form method="POST" action="{{ url('adminpanel/blog') }}" enctype="multipart/form-data">

                            @csrf

                                @include('admin.blog.form')

                        </form>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            </div>
            <!-- /.row -->
    </section>


@endsection


@section('footer')
    <!-- Select2 -->
    {!! Html::script('/admin/bower_components/select2/dist/js/select2.full.min.js') !!}
    <script>
        $(".select_tags").select2({
            dir: "rtl",
        });
    </script>
@endsection