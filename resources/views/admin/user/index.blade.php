@extends('admin.layouts.layout')

@section('title')
    المستخدمين
@endsection

@section('header')
    {!! Html::style('/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
@endsection

@section('content')

    <section class="content-header">
        <h1>
           المستخدمين
            <small>عرض المستخدمين</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> الرئيسيه</a></li>
            <li class="active">عرض المستخدمين</li>
        </ol>



    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">اسم المستخدم</th>
                                <th class="text-center">البريد الالكترونى</th>
                                <th class="text-center">اضيف فى </th>
                                <th class="text-center">الصلاحيات</th>
                                <th class="text-center">التحكم</th>
                            </tr>
                            </thead>
                            <tbody>
                @foreach($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->created_at}}</td>
                                <td>
                                    {{$user->admin == 1 ? 'مدير' : 'مستخدم'}}
                                </td>

                                <td>
                                <a  class="btn btn-info" href="{{url('/adminpanel/users/'.$user->id.'/edit')}}">
                                    <i class="fa fa-pencil-square" ></i>
                                </a>

                                <a  class="btn btn-danger" onclick="return confirm('هل انت متأكد ؟!')" href="{{url('/adminpanel/users/'.$user->id.'/delete')}}">
                                    <i class="fa fa-trash" ></i>
                                </a>
                                </td>
                            </tr>
                    @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->


@endsection


@section('footer')
    <!-- DataTables -->
    {!! Html::script('/admin/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}


    <script>
        $(function () {

            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        })
    </script>
@endsection