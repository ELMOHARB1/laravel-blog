

    <div class="form-group row">
        <label style="float: none;" for="name" class="col-md-6 text-right">الاسم</label>

        <div style="float: none;" class="col-md-6">
            {!! Form::text("name",null,['class'=>'form-control']) !!}

            @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div  class="form-group row">
        <label style="float: none;" for="email" class="col-md-6 text-right">البريد الالكترونى</label>

        <div style="float: none;" class="col-md-6">
            {!! Form::email("email",null,['class'=>'form-control']) !!}

            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div  class="form-group row">
        <label style="float: none;" for="admin" class="col-md-6 text-right">الصلاحيات</label>

        <div style="float: none;" class="col-md-6">
            {!! Form::select("admin",[0=>'user',1=>'admin'],null,['class'=>'form-control']) !!}

            @if ($errors->has('admin'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('admin') }}</strong>
                </span>
            @endif
        </div>
    </div>
@if(!isset($user))
    <div class="form-group row">
        <label style="float: none;" for="password" class="col-md-6 text-right">كلمة المرور</label>

        <div style="float: none;" class="col-md-6">
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label style="float: none;" for="password-confirm" class="col-md-6 text-right">تأكيد كلمة المرور</label>

        <div style="float: none;" class="col-md-6">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
        </div>
    </div>

@endif


    <div class="form-group row mb-0">
        <div style="float: none;"  class="col-md-6 offset-md-4">
            <button type="submit" class="submit">
                حفظ
            </button>
        </div>
    </div>


