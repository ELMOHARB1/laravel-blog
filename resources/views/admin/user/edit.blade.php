
@extends('admin.layouts.layout')

@section('title')
    تعديل المستخدم
    {{$user->name}}
@endsection

@section('header')

@endsection

@section('content')

    <section class="content-header">
        <h1>
            المستخدمين
            <small>   تعديل المستخدم  {{ $user->name }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> الرئيسيه</a></li>
            <li><a href="{{url('/adminpanel/users')}}"><i class="fa fa-users"></i> عرض المستخدمين</a></li>

            <li class="active">
                  تعديل المستخدم</li>
            {{$user->name}}
        </ol>
    </section>


    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">تعديل بيانات المستخدم</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">


                       {!! Form::model($user,array('route' =>array('users.update',$user->id), 'method'=>'patch')) !!}

                            @include('admin.user.form')

                       {!! Form::close() !!}
<hr>



                        <section class="content">
                            <div class="row">
                                <div class="col-xs-12">

                                    <div class="box">
                                        <div class="box-header">
                                            <h3 class="box-title"> تعديل كلمة مرور المستخدم</h3>
                                        </div>
                                        <div class="box-body">

          {!! Form::open(array('url' => 'adminpanel/users/changePassword')) !!}

                     <input type="hidden" name="user_id" value="{{ $user->id }}">
                        <div class="form-group row">
                            <label style="float: none;" for="password" class="col-md-6 text-right">كلمة المرور</label>

                            <div style="float: none;" class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div style="float: none;"  class="col-md-6 offset-md-4">
                                <button type="submit" class="submit">
                                    تعديل
                                </button>
                            </div>
                        </div>

                 {!! Form::close() !!}

                                        </div> </div> </div> </div> </section>




                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
        <!-- /.row -->
    </section>


@endsection


@section('footer')

@endsection