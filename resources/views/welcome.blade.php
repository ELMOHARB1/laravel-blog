@extends('layouts.app')
@section('title')
اهلا بك معنا
@endsection




@section('content')
    <div class="banner text-center">
        <div class="container">
            <div class="banner-info">
                <h1>اهلا بك فى المدونه</h1>
                <p>يمكنك متابعة كل ما هو جديد معنا
                قم برؤية اخر التدوينات<br> من هنا :</p>
                <a class="banner_btn" href="{{url('blog')}}">المدونه</a>
            </div>
        </div>
    </div>
    <div class="main">


        @if(!empty($blogs))
        <div class="content_white">
            <h2>التدوينات الحديثه</h2>
            <p>يمكنك رؤية المزيد من التدوينات من خلال صفحة العرض</p>
        </div>



        <div class="featured_content" id="feature">
            <div class="container">
                <div class="row text-center">

                    <?php
                    $numItems = count($blogs);
                    $i = 0;
                    ?>

                    @foreach($blogs as $blog)

                    <div class="col-mg-3 col-xs-3 @if(++$i !== $numItems)feature_grid1 @else feature_grid2 @endif "> <img  class="img-bordered img-circle" width="100" height="100" src="{{url('images/'.$blog->image)}}" >
                        <h3 style="height: 90px;width: 250px;" class="m_1"><a style="height: 62px;width: 190px;" href="services.html">{!! \Illuminate\Support\Str::words($blog->title,6,'..')!!}</a></h3>
                        <p style="height: 50px;width: 245px;"  class="m_2">{!! \Illuminate\Support\Str::words($blog->short_desc,12,'...')!!}</p>
                        <a href="{{url('blog/details/'.$blog->id)}}" class="feature_btn">المزيد</a> </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif


        <div class="about-info">
                <div class="container">
                    <div class="row">


                        <div class="col-md-10">
                            <div class="block-heading-two">
                                <h2><span>عن الموقع</span></h2>
                            </div>
                            <p>
                                هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم، ولكن الغالبية تم تعديلها بشكل ما عبر إدخال بعض النوادر أو الكلمات العشوائية إلى النص. إن كنت تريد أن تستخدم نص لوريم إيبسوم ما، عليك أن تتحقق أولاً أن ليس هناك أي كلمات أو عبارات محرجة أو غير لائقة مخبأة في هذا النص. بينما تعمل جميع مولّدات نصوص لوريم إيبسوم على الإنترنت على إعادة تكرار مقاطع من نص لوريم إيبسوم نفسه عدة مرات بما تتطلبه الحاجة، يقوم مولّدنا هذا باستخدام كلمات من قاموس يحوي على أكثر من 200 كلمة لا تينية، مضاف إليها مجموعة من الجمل النموذجية، لتكوين نص لوريم إيبسوم ذو شكل منطقي قريب إلى النص الحقيقي. وبالتالي يكون النص الناتح خالي من التكرار، أو أي كلمات أو عبارات غير لائقة أو ما شابه. وهذا ما يجعله أول مولّد نص لوريم إيبسوم حقيقي على الإنترنت.
                            </p>

                            {{--<a class="banner_btn" href="about.html">Read More</a> --}}
                        </div>

                        <div class="col-md-2">
                            <img  class="img-bordered" width="100%" height="100%" src="images/CATPLE_WALLPAPERS_9366.jpg" >
                        </div>

                    </div>
                </div>
            </div>
    </div>


@endsection


