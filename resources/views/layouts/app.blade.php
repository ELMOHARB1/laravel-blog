<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! Html::style('/website/css/bootstrap.min.css')!!}
    {!! Html::style('/website/css/flexslider.css')!!}
    {!! Html::style('/website/css/style.css')!!}
    {!! Html::style('/website/css/font-awesome.min.css')!!}
    {!! Html::style('/website/css/jquery.toast.min.css')!!}
    {!! Html::script('/website/js/jquery.min.js')!!}

    <style type="text/css">
        .my-active span{
            background-color: #2ABB9B !important;
            color: white !important;
            border-color: #2ABB9B !important;
        }
    </style>


    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900' rel='stylesheet' type='text/css'>

    <title> موقع مدونه
|
           @yield('title')
    </title>

    <header>
      @yield('header')

    </header>

</head>
<body style="direction: rtl;">



    <div id="app">

        <div class="header">
            <div class="container"> <a class="navbar-brand" style="float: right;" href="{{url('/')}}"><i class="fa fa-newspaper-o"></i> Blog</a>
                <div class="menu pull-left" > <a class="toggleMenu" href="#"><img src="{{Request::root()}}/website/images/nav_icon.png" alt="" /> </a>
                    <ul class="nav" id="nav">

                        <li @if(Request::segment(1) == 'home' || Request::segment(1) == '') class="current" @endif ><a href="{{ url('/home') }}">الرئيسيه</a></li>
                        <li @if(Request::segment(1) == 'blog')) class="current" @endif ><a href="{{url('blog')}}">المدونه</a></li>
                        <?php $categories = \App\Category::all(); ?>
                        @if(!empty($categories))
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">الأقسام<span class="caret"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right">

                                @foreach($categories as $cat)
                                <li><a href="{{url('blog/'.$cat->name)}}">{{$cat->name}}</a></li><br>
                                @endforeach

                            </ul>
                        </li>
                        @endif
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                البحث <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                <!--  redirect url search -->

                                <form class="navbar-form navbar-left" method="post">
                                    <div class="input-group">
                                        <input id="search_text" style="border-bottom-left-radius: 0;border-top-left-radius: 0;" type="search" class="form-control" placeholder="بحث.." name="search">
                                        <div class="input-group-btn">
                                            <button id="do_search"  style="border-bottom-right-radius: 0;border-top-right-radius: 0;" class="btn btn-default" >
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>

                                </form>



                                 <!--  form action post search -->

                                {{--<form class="navbar-form navbar-left" action="{{url('search')}}" method="post" role="search">--}}
                                      {{--@csrf--}}
                                    {{--<div class="input-group">--}}
                                        {{--<input style="border-bottom-left-radius: 0;border-top-left-radius: 0;" type="search" class="form-control" placeholder="بحث.." name="search">--}}
                                        {{--<div class="input-group-btn">--}}
                                            {{--<button style="border-bottom-right-radius: 0;border-top-right-radius: 0;" class="btn btn-default" type="submit">--}}
                                                {{--<i class="fa fa-search"></i>--}}
                                            {{--</button>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                {{--</form>--}}

                            </div>
                        </li>


                        @guest
                            {{--<li class="nav-item">--}}
                                {{--<a class="nav-link" href="{{ route('login') }}">تسجيل الدخول</a>--}}
                            {{--</li>--}}
                            {{--<li class="nav-item">--}}
                                {{--@if (Route::has('register'))--}}
                                    {{--<a class="nav-link" href="{{ route('register') }}">انشاء حساب</a>--}}
                                {{--@endif--}}
                            {{--</li>--}}
                        @else

                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       تسجيل الخروج
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>

                        @endguest
                        <div class="clear"></div>




                    </ul>


                </div>
            </div>
        </div>



            @yield('content')

        <div class="footer">
            <div class="footer_bottom">
                <div class="follow-us"> <a class="fa fa-facebook social-icon" href="#"></a> <a class="fa fa-twitter social-icon" href="#"></a> <a class="fa fa-linkedin social-icon" href="#"></a> <a class="fa fa-google-plus social-icon" href="#"></a> </div>
                <div class="copy">
                    <p>Copyright &copy; 2019</p>
                </div>
            </div>
        </div>


    </div>

    {!! Html::script('/website/js/responsive-nav.js')!!}
    {!! Html::script('/website/js/bootstrap.min.js')!!}
    {!! Html::script('/website/js/jquery.flexslider.js')!!}
    {!! Html::script('/website/js/jquery.toast.min.js')!!}


    @yield('footer')

    <script>
        $("#do_search").click(function(ev){
            ev.preventDefault();
            location.replace('{{url('/blog')}}/'+$('#search_text').val());
        });
    </script>




</body>
</html>
